ARG HELM_VERSION
ARG KUBERNETES_VERSION

FROM "registry.gitlab.com/dontyvir/helm-install-image/releases/${HELM_VERSION}-kube-${KUBERNETES_VERSION}"

RUN apk add --no-cache bash

COPY src/ build/

RUN ln -s /aws/build/bin/auto-deploy /usr/local/bin/auto-deploy
